


async function addStudentToServer(studentData) {
    try {
        const response = await fetch("checkInfo.php", {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify(studentData)
        });
        const obj = await response.json();
        console.log(obj);

        if (obj.success === true) {
        }

        return obj;
    } catch (error) {
        throw error;
    }
}
document.addEventListener('DOMContentLoaded', async function () {
    try {
        const response = await fetch('checkInfo.php', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }
        });
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        const data = await response.json();
        console.log(data); 

        let table = document.querySelector('table'); 

       
        while (table.rows.length > 1) {
            table.deleteRow(1);
        }

        data.data.forEach(student => {
            
            var row = table.insertRow();
            row.dataset.id = student.id;
    
            var checkboxCell = row.insertCell();
            var checkbox = document.createElement('input');
            checkbox.type = 'checkbox';
            checkboxCell.appendChild(checkbox);

            row.insertCell().textContent = student.groupname;
            row.insertCell().textContent = student.firstname + ' ' + student.lastname;
            row.insertCell().textContent = student.gender;
            row.insertCell().textContent = student.birthday;

            let statusCell = row.insertCell();
            let radioBtn = document.createElement('input');
            radioBtn.type = 'radio';
            radioBtn.name = 'status-student';
            statusCell.appendChild(radioBtn);

            let optionsCell = row.insertCell();
            optionsCell.innerHTML = '<div class="button-container"><button style="border: none; background-color: #fff;font-size: 23px;"><i class="bi bi-pencil-square"></i></button><button style="border: none; background-color: #fff;font-size: 23px; "><i class="bi bi-x-square"></i></button></div>';

            createDeleteIcons();
            createEditIcons();
        });
    } catch (error) {
        console.error('Error:', error);
    }
});





document.getElementById('addStudent').onclick = function () {
    document.getElementById('id-window').style.display = "block";
    document.getElementById('create-student-btn').style.display = "block";
    document.getElementById('edit-student-btn').style.display = "none";
    document.getElementById('formHeader').textContent = `Add student`;
}

const editBtn = document.getElementById('edit-student-btn');
createDeleteIcons();

const createStudent_btn = document.getElementById('create-student-btn');
async function createStudent(event) {
    event.preventDefault();
    let group = document.getElementById('group-name').value;
    let firstname = document.getElementById('firstname-student').value;
    let lastname = document.getElementById('lastname-student').value;
    let gender = document.getElementById('gender-student').value;
    let birthday = document.getElementById('bday').value;

    if (validateName(firstname) && validateName(lastname)) {
        if (birthday == "") {
            window.alert("Please enter the birthday.");
            return;
        }

        if (group == "Select Group") {
            window.alert("Please select the group.");
            return;
        }
        if (gender == "Select Gender") {
            window.alert("Please select the gender.");
            return;
        }
    
    let student = {
        id: Math.floor(Math.random() * 10000),
        group: group,
        firstname: firstname,
        lastname: lastname,
        gender: gender,
        birthday: birthday
    };

      try{

        const obj = await addStudentToServer(student);
       
        if (obj.success === true) {
      
     

        let table = document.querySelector('table');
        let newRow = table.insertRow(-1);
        newRow.dataset.id = student.id;
        let checkboxCell = newRow.insertCell(0);
        checkboxCell.innerHTML = '<input type="checkbox" style="transform: scale(1.2);">';

        let groupCell = newRow.insertCell(1);
        groupCell.textContent = group;

        let nameCell = newRow.insertCell(2);
        nameCell.textContent = firstname + " " + lastname;

        let genderCell = newRow.insertCell(3);
        genderCell.textContent = gender;

        let birthdayCell = newRow.insertCell(4);
        birthdayCell.textContent = birthday;

        let statusCell = newRow.insertCell(5);
        let radioBtn = document.createElement('input');
        radioBtn.type = 'radio';
        radioBtn.name = 'status-student';
        statusCell.appendChild(radioBtn);

        let optionsCell = newRow.insertCell(6);
        optionsCell.innerHTML = '<div class="button-container"><button style="border: none; background-color: #fff;font-size: 23px;"><i class="bi bi-pencil-square"></i></button><button style="border: none; background-color: #fff;font-size: 23px; "><i class="bi bi-x-square"></i></button></div>';

        createDeleteIcons();
        createEditIcons();

        document.getElementById('id-window').style.display = "none";
      }
    
    }
      catch(error){
        console.error('Error creating student:', error);  
    }
    } else {
       window.alert("The first or last name are incorrect");
  }
};

function createDeleteIcons() {
    let deleteIcons = document.querySelectorAll('.bi-x-square');
    deleteIcons.forEach(function (icon) {
        icon.addEventListener('click', function () {
            document.getElementById('delete-confirm-window').style.display = 'block';
            let closeBtn = document.getElementById('close-delete-window-btn');
            let confirmBtn = document.getElementById('confirm-delete-btn');
            let cancelBtn = document.getElementById('cancel-delete-btn');


            confirmBtn.onclick = async function deleteStudent (event) {
              
                    event.preventDefault();
            
                    var row = icon.closest('tr');
                
                    let deletedStudent = {
                        id: row.dataset.id,
                        group: "",
                        firstname: "",
                        lastname: "",
                        gender: "",
                        birthday: ""
                    };
                
                
                    try {
                        const obj = await addStudentToServer(deletedStudent);
                       
                      
                            row.remove();
                
                          
                        
                    } catch (error) {
                        console.error('Error deleting student:', error);
                    }
                
                    document.getElementById('delete-confirm-window').style.display = 'none';
                
            };

            closeBtn.onclick = function () {
                document.getElementById('delete-confirm-window').style.display = 'none';
            }

            cancelBtn.addEventListener('click', function () {
                document.getElementById('delete-confirm-window').style.display = 'none';
            });
        });
    });
}



function validateName(name) {
    let nameRegex = /^[A-Z][a-z]+(-[A-Z][a-z]+)?$/;

    return nameRegex.test(name);
}

function openEditWindow() {
   

    document.getElementById('id-window').style.display = 'block';
    document.getElementById('formHeader').textContent = `Edit student`;
    document.getElementById('edit-student-btn').style.display = 'block';
    document.getElementById('create-student-btn').style.display = 'none';

    let selectedRow = event.target.closest('tr');

    let group = selectedRow.cells[1].textContent;
    
    let fullName = selectedRow.cells[2].textContent;
    let gender = selectedRow.cells[3].textContent;
    let birthday = selectedRow.cells[4].textContent;
    
    let nameParts = fullName.split(' ');
    let firstname = nameParts[0];
    let lastname = nameParts.slice(1).join(' ');
    document.getElementById('group-name').value = group;
    document.getElementById('firstname-student').value = firstname;
    document.getElementById('lastname-student').value = lastname;
    document.getElementById('gender-student').value = gender;
    document.getElementById('bday').value = birthday;
   
    editBtn.onclick = async function editStudents(event){
        event.preventDefault();

       
        let editedStudent = {
            id: selectedRow.dataset.id,
            group: document.getElementById('group-name').value,
            firstname: document.getElementById('firstname-student').value,
            lastname: document.getElementById('lastname-student').value,
            gender: document.getElementById('gender-student').value,
            birthday: document.getElementById('bday').value
        };
    if (!validateName(editedStudent.firstname) || !validateName(editedStudent.lastname)) {
        window.alert("Please enter valid first and last names.");
            return;
        }
    
        if (editedStudent.birthday == "") {
            window.alert("Please enter the birthday.");
            return;
        }
       

        try{

            const obj = await addStudentToServer(editedStudent);
    
            if (obj.success === true) {
      
       
  
            selectedRow.cells[1].textContent = editedStudent.group;
            selectedRow.cells[2].textContent = editedStudent.firstname + " " + editedStudent.lastname;
            selectedRow.cells[3].textContent = editedStudent.gender;
            selectedRow.cells[4].textContent = editedStudent.birthday;
        

    

      
    }
}
catch(error){
    console.error('Error editing student:', error);  
}
     document.getElementById('id-window').style.display = "none";

}
}

function createEditIcons() {
    let editIcons = document.querySelectorAll('.bi-pencil-square');
    editIcons.forEach(function (icon) {
        icon.addEventListener('click', openEditWindow);
    });
}



document.getElementById('close-id-window-btn').onclick = function () {
    document.getElementById('id-window').style.display = 'none';
};

document.getElementById('delete-confirm-window').onclick = function () {
    document.getElementById('delete-confirm-window').style.display = 'none';
};




