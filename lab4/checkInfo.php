<?php
$db_server = "localhost";
$db_user = "root";
$db_pass = "";
$db_name = "student_db";

try {
    $conn = mysqli_connect($db_server, $db_user, $db_pass, $db_name);
} catch (mysqli_sql_exception $e) {
    $response = array(
        'message' => 'Connection failed: ' . mysqli_connect_error(),
        'success' => false,
        'data' => null,
    );
    http_response_code(500);
    echo json_encode($response);
    exit;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $object = json_decode(file_get_contents("php://input"), true);
    $id = $object['id'];

    if (isset($object['id']) && !empty($object['group'])) {
        $group = $object['group'];
        $firstname = $object['firstname'];
        $lastname = $object['lastname'];
        $gender = $object['gender'];
        $birthday = $object['birthday'];

        if (empty($group) || $group == "Select Group") {
            $response = array(
                'message' => 'Please enter a group',
                'success' => false,
                'data' => null,
            );
            http_response_code(400);
            echo json_encode($response);
            exit;
        }

        if (empty($firstname)) {
            $response = array(
                'message' => 'Please enter your first name',
                'success' => false,
                'data' => null,
            );
            http_response_code(400);
            echo json_encode($response);
            exit;
        }

        if (empty($lastname)) {
            $response = array(
                'message' => 'Please enter your last name',
                'success' => false,
                'data' => null,
            );
            http_response_code(400);
            echo json_encode($response);
            exit;
        }

        if (!validateName($firstname) || !validateName($lastname)) {
            $response = array("success" => false, "message" => "Invalid first or last name.", 'data' => null);
            http_response_code(400);
            echo json_encode($response);
            exit;
        }

        if (empty($gender) || $gender == "Select Gender") {
            $response = array(
                'message' => 'Please enter your gender',
                'success' => false,
                'data' => null,
            );
            http_response_code(400);
            echo json_encode($response);
            exit;
        }

        if (empty($birthday)) {
            $response = array(
                'message' => 'Please enter your birthday',
                'success' => false,
                'data' => null,
            );
            http_response_code(400);
            echo json_encode($response);
            exit;
        }

        if (!validateBirthday($birthday)) {
            $response = array("success" => false, "message" => "Invalid date of birth", 'data' => null);
            http_response_code(400);
            echo json_encode($response);
            exit;
        }

        $sql_check = "SELECT id FROM students WHERE id='$id'";
        $result_check = mysqli_query($conn, $sql_check);

        if (mysqli_num_rows($result_check) > 0) {
            $sql = "UPDATE students SET groupname='$group', firstname='$firstname', lastname='$lastname', gender='$gender', birthday='$birthday' WHERE id='$id'";
        } else {
            $sql = "INSERT INTO students (id, groupname, firstname, lastname, gender, birthday) VALUES ('$id', '$group', '$firstname', '$lastname', '$gender', '$birthday')";
        }
        if (mysqli_query($conn, $sql)) {
            $response = array(
                'message' => mysqli_num_rows($result_check) > 0 ? 'Student data has been successfully edited' : 'Student data has been successfully added',
                'success' => true,
                'data' => $object
            );
            header('Content-Type: application/json');
            echo json_encode($response);
        } else {
            $response = array(
                'message' => 'Error: ' . mysqli_error($conn),
                'success' => false,
                'data' => null
            );
            http_response_code(500);
            echo json_encode($response);
        }
    } else {
        $sql = "DELETE FROM students WHERE id='$id'";

        if (mysqli_query($conn, $sql)) {
            $response = array(
                'message' => 'Student data has been successfully deleted',
                'success' => true,
                'data' => $object
            );
            header('Content-Type: application/json');
            echo json_encode($response);
        } else {
            $response = array(
                'message' => 'Error deleting data: ' . mysqli_error($conn),
                'success' => false,
                'data' => null
            );
            header('Content-Type: application/json');
            http_response_code(500);
            echo json_encode($response);
        }
    }
} else {
    $sql = "SELECT * FROM students";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        $response = array(
            'message' => 'Дані з бази даних',
            'success' => true,
            'data' => array(),
        );

        while ($row = mysqli_fetch_assoc($result)) {
            $response['data'][] = $row;
        }

        http_response_code(200); 
        echo json_encode($response);
    } else {
        $response = array(
            'message' => 'Немає даних в базі даних',
            'success' => false,
            'data' => array(),
        );

        http_response_code(404); 
        echo json_encode($response);
    }
}

function validateName($name) {
    $nameRegex = "/^[A-Z][a-z]+(-[A-Z][a-z]+)?$/";
    return preg_match($nameRegex, $name);
}

function validateBirthday($birthday) {
    $dateParts = explode('-', $birthday);
    $year = intval($dateParts[0]);

    if ($year < 1950 || $year > 2010) {
        return false;
    }

    return true;
}

mysqli_close($conn);
?>
