document.getElementById('addStudent').onclick = function () {
    document.getElementById('id-window').style.display = "block";
}
createDeleteIcons();
document.getElementById('create-student-btn').onclick = function () {
    let group = document.getElementById('group-name').value;
    let name = document.getElementById('name-student').value;
    let gender = document.getElementById('gender-student').value;
    let birthday = document.getElementById('bday').value;
    let status = document.querySelector('input[name="status-student"]:checked').value;

    console.log('Selected Status:', status);
    let table = document.querySelector('table');
    let newRow = table.insertRow(-1);

    let checkboxCell = newRow.insertCell(0);
    checkboxCell.innerHTML = '<input type="checkbox" style="transform: scale(1.2);">';

    let groupCell = newRow.insertCell(1);
    groupCell.textContent = group;

    let nameCell = newRow.insertCell(2);
    nameCell.textContent = name;

    let genderCell = newRow.insertCell(3);
    genderCell.textContent = gender;

    let birthdayCell = newRow.insertCell(4);
    birthdayCell.textContent = birthday;
 
 let statusCell = newRow.insertCell(5);
let radioBtn = document.createElement('input');
radioBtn.type = 'radio';
radioBtn.name = 'status-student';
radioBtn.value = status;
radioBtn.checked = status === 'active';  
statusCell.appendChild(radioBtn);

    let optionsCell = newRow.insertCell(6);
    optionsCell.innerHTML = '<div class="button-container"><button style="border: none; background-color: #fff;font-size: 23px;"><i class="bi bi-pencil-square"></i></button><button style="border: none; background-color: #fff;font-size: 23px; "><i class="bi bi-x-square"></i></button></div>';

    createDeleteIcons();
    document.getElementById('id-window').style.display = "none";
}


function createDeleteIcons() {
    let deleteIcons = document.querySelectorAll('.bi-x-square');
    deleteIcons.forEach(function (icon) {
        icon.addEventListener('click', function () {
            document.getElementById('delete-confirm-window').style.display = 'block';
            let confirmBtn = document.getElementById('confirm-delete-btn');
            let cancelBtn = document.getElementById('cancel-delete-btn');
            confirmBtn.addEventListener('click', function () {
                var row = icon.closest('tr');
                row.remove();
                document.getElementById('delete-confirm-window').style.display = 'none';
            });

            cancelBtn.addEventListener('click', function () {
                document.getElementById('delete-confirm-window').style.display = 'none';
            });
        });
    });
}
function openEditWindow() {
   
    document.getElementById('edit-window').style.display = 'block';


    let selectedRow = this.closest('tr'); 
    let group = selectedRow.cells[1].textContent;
    let name = selectedRow.cells[2].textContent;
    let gender = selectedRow.cells[3].textContent;
    let birthday = selectedRow.cells[4].textContent;

    
    document.getElementById('edit-group-name').value = group;
    document.getElementById('edit-name-student').value = name;
    document.getElementById('edit-gender-student').value = gender;
    document.getElementById('edit-bday').value = birthday;

   let status = selectedRow.cells[5].innerHTML.includes('active') ? 'active' : 'inactive';
    document.querySelector('input[name="edit-status-student"][value="' + status + '"]').checked = true;
}


let editIcons = document.querySelectorAll('.bi-pencil-square');
editIcons.forEach(function (icon) {
    icon.addEventListener('click', openEditWindow);
});

document.getElementById('save-changes-btn').onclick = function () {
   
    let editedGroup = document.getElementById('edit-group-name').value;
    let editedName = document.getElementById('edit-name-student').value;
    let editedGender = document.getElementById('edit-gender-student').value;
    let editedBirthday = document.getElementById('edit-bday').value;
    let editedStatus = document.querySelector('input[name="status-student"]:checked').value;

    
    let table = document.querySelector('table');
    let rowToUpdate = table.querySelector(`tr[data-row-id="${rowId}"]`);
    rowToUpdate.cells[1].textContent = editedGroup;
    rowToUpdate.cells[2].textContent = editedName;
    rowToUpdate.cells[3].textContent = editedGender;
    rowToUpdate.cells[4].textContent = editedBirthday;

    
    rowToUpdate.cells[5].innerHTML = '<div class="status-indicator ' + (editedStatus === 'active' ? 'active' : 'inactive') + '"></div>';
  
    rowToUpdate.cells[6].innerHTML = '<div class="button-container"><button style="border: none; background-color: #fff;font-size: 23px;"><i class="bi bi-pencil-square"></i></button><button style="border: none; background-color: #fff;font-size: 23px; "><i class="bi bi-x-square"></i></button></div>';

    createDeleteIcons();

    document.getElementById('edit-window').style.display = 'none';
};


document.getElementById('close-id-window-btn').onclick = function () {
   
    document.getElementById('id-window').style.display = 'none';
};

  document.getElementById('close-edit-window-btn').onclick = function () {
   
    document.getElementById('edit-window').style.display = 'none'; 
};
document.getElementById('delete-confirm-window').onclick = function () {
  
    document.getElementById('delete-confirm-window').style.display = 'none';
};
