const CACHE_NAME = 'my-pwa-cache'; 
const urlsToCache = [
 '/', 
'/script.js',
 '/index.html', 
'/style.css' 
]; 
self.addEventListener('install', function(event)  { 
event.waitUntil( caches.open(CACHE_NAME) .then(function(cache)
 { console.log('Opened cache'); return cache.addAll(urlsToCache); }) ); });
 