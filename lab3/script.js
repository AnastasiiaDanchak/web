let students = [];


async function addStudentToServer(studentData) {
    try {
        const response = await fetch("addStudent.php", {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify(studentData)
        });
        const obj = await response.json();
        console.log(obj);

        if (obj.success === true) {
            console.log('Student data has been successfully added');
        }

        return obj;
    } catch (error) {
        console.error('Error adding student:', error);
        throw error;
    }
}


document.getElementById('addStudent').onclick = function () {
    document.getElementById('id-window').style.display = "block";
    document.getElementById('create-student-btn').style.display = "block";
    document.getElementById('edit-student-btn').style.display = "none";
    document.getElementById('formHeader').textContent = `Add student`;
}

const editBtn = document.getElementById('edit-student-btn');
createDeleteIcons();

const createStudent_btn = document.getElementById('create-student-btn');
async function createStudent(event) {
    event.preventDefault();
    let group = document.getElementById('group-name').value;
    let firstname = document.getElementById('firstname-student').value;
    let lastname = document.getElementById('lastname-student').value;
    let gender = document.getElementById('gender-student').value;
    let birthday = document.getElementById('bday').value;

    if (validateName(firstname) && validateName(lastname)) {
        if (birthday == "") {
            window.alert("Please enter the birthday.");
            return;
        }

        if (group == "Select Group") {
            window.alert("Please select the group.");
            return;
        }
        if (gender == "Select Gender") {
            window.alert("Please select the gender.");
            return;
        }
    
    let student = {
        id: Date.now().toString(),
        group: group,
        firstname: firstname,
        lastname: lastname,
        gender: gender,
        birthday: birthday
    };
   
      try{

        const obj = await addStudentToServer(student);

        if (obj.success === true) {
        students.push(student);
        let jsonStudent = JSON.stringify(student);
        console.log('Student added (JSON):');
        console.log(jsonStudent);

        let table = document.querySelector('table');
        let newRow = table.insertRow(-1);
        newRow.dataset.id = student.id;
        let checkboxCell = newRow.insertCell(0);
        checkboxCell.innerHTML = '<input type="checkbox" style="transform: scale(1.2);">';

        let groupCell = newRow.insertCell(1);
        groupCell.textContent = group;

        let nameCell = newRow.insertCell(2);
        nameCell.textContent = firstname + " " + lastname;

        let genderCell = newRow.insertCell(3);
        genderCell.textContent = gender;

        let birthdayCell = newRow.insertCell(4);
        birthdayCell.textContent = birthday;

        let statusCell = newRow.insertCell(5);
        let radioBtn = document.createElement('input');
        radioBtn.type = 'radio';
        radioBtn.name = 'status-student';
        statusCell.appendChild(radioBtn);

        let optionsCell = newRow.insertCell(6);
        optionsCell.innerHTML = '<div class="button-container"><button style="border: none; background-color: #fff;font-size: 23px;"><i class="bi bi-pencil-square"></i></button><button style="border: none; background-color: #fff;font-size: 23px; "><i class="bi bi-x-square"></i></button></div>';

        createDeleteIcons();
        createEditIcons();

        document.getElementById('id-window').style.display = "none";
      }
    }
      catch(error){
        console.error('Error creating student:', error);  
    }
    } else {
       window.alert("The first or last name are incorrect");
  }
};

function createDeleteIcons() {
    let deleteIcons = document.querySelectorAll('.bi-x-square');
    deleteIcons.forEach(function (icon) {
        icon.addEventListener('click', function () {
            document.getElementById('delete-confirm-window').style.display = 'block';
            let closeBtn = document.getElementById('close-delete-window-btn');
            let confirmBtn = document.getElementById('confirm-delete-btn');
            let cancelBtn = document.getElementById('cancel-delete-btn');

            var row = icon.closest('tr');

            confirmBtn.addEventListener('click', function () {
                let studentId = row.dataset.id;

                const index = students.findIndex(s => s.id === studentId);

                if (index !== -1) {
                    let deletedStudent = students.splice(index, 1)[0];
                    row.remove();

                    let jsonStudent = JSON.stringify(deletedStudent);
                    console.log('Student deleted (JSON):');
                    console.log(jsonStudent);
                }

                document.getElementById('delete-confirm-window').style.display = 'none';
            });

            closeBtn.onclick = function () {
                document.getElementById('delete-confirm-window').style.display = 'none';
            }

            cancelBtn.addEventListener('click', function () {
                document.getElementById('delete-confirm-window').style.display = 'none';
            });
        });
    });
}

function validateName(name) {
    let nameRegex = /^[A-Z][a-z]+(-[A-Z][a-z]+)?$/;

    return nameRegex.test(name);
}

function openEditWindow() {
   

    document.getElementById('id-window').style.display = 'block';
    document.getElementById('formHeader').textContent = `Edit student`;
    document.getElementById('edit-student-btn').style.display = 'block';
    document.getElementById('create-student-btn').style.display = 'none';

    let selectedRow = event.target.closest('tr');
 

    let group = selectedRow.cells[1].textContent;
    
    let fullName = selectedRow.cells[2].textContent;
    let gender = selectedRow.cells[3].textContent;
    let birthday = selectedRow.cells[4].textContent;
    
    let nameParts = fullName.split(' ');
    let firstname = nameParts[0];
    let lastname = nameParts.slice(1).join(' ');
    document.getElementById('group-name').value = group;
    document.getElementById('firstname-student').value = firstname;
    document.getElementById('lastname-student').value = lastname;
    document.getElementById('gender-student').value = gender;
    document.getElementById('bday').value = birthday;
   
    editBtn.onclick = async function editStudents(event){
        event.preventDefault();

       
        let editedStudent = {
            id: selectedRow.dataset.id,
            group: document.getElementById('group-name').value,
            firstname: document.getElementById('firstname-student').value,
            lastname: document.getElementById('lastname-student').value,
            gender: document.getElementById('gender-student').value,
            birthday: document.getElementById('bday').value
        };
    if (!validateName(editedStudent.firstname) || !validateName(editedStudent.lastname)) {
        window.alert("Please enter valid first and last names.");
            return;
        }
    
        if (editedStudent.birthday == "") {
            window.alert("Please enter the birthday.");
            return;
        }
       
        const index = students.findIndex(s => s.id === editedStudent.id);
        try{

            const obj = await addStudentToServer(editedStudent);
    
            if (obj.success === true) {
        if (index !== -1) {
            students[index] = editedStudent;
    
            selectedRow.cells[1].textContent = editedStudent.group;
            selectedRow.cells[2].textContent = editedStudent.firstname + " " + editedStudent.lastname;
            selectedRow.cells[3].textContent = editedStudent.gender;
            selectedRow.cells[4].textContent = editedStudent.birthday;
        }

    
        let jsonStudent = JSON.stringify(editedStudent);
        console.log('Student edited (JSON):');
        console.log(jsonStudent);
        console.log('Student data has been successfully edited')
    }
}
catch(error){
    console.error('Error editing student:', error);  
}
     document.getElementById('id-window').style.display = "none";

}
}

function createEditIcons() {
    let editIcons = document.querySelectorAll('.bi-pencil-square');
    editIcons.forEach(function (icon) {
        icon.addEventListener('click', openEditWindow);
    });
}



document.getElementById('close-id-window-btn').onclick = function () {
    document.getElementById('id-window').style.display = 'none';
};

document.getElementById('delete-confirm-window').onclick = function () {
    document.getElementById('delete-confirm-window').style.display = 'none';
};
