<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $object = json_decode(file_get_contents("php://input"), true);
 
 

   
    if (!isset($object['group']) || empty($object['group']) || $object['group']=="Select Group") {
        header("HTTP/1.1 500 Internal Server Error");
        $response = array(
            'message' => 'Please enter a group',
            'success' => false,
            'data'=>null,
        );
        http_response_code(400); 
        echo json_encode($response);
        exit;
    } 

   
    if (!isset($object['firstname']) || empty($object['firstname'])) {
        header("HTTP/1.1 500 Internal Server Error");
        $response = array(
            'message' => 'Please enter your first name',
            'success' => false,
            'data'=>null,
        );
        http_response_code(400); 
        echo json_encode($response);
        exit;
    } 


    if (!isset($object['lastname']) || empty($object['lastname'])) {
        header("HTTP/1.1 500 Internal Server Error");
        $response = array(
            'message' => 'Please enter your last name',
            'success' => false,
            'data'=>null,
        );
        http_response_code(400); 
        echo json_encode($response);
        exit;
    }

    if (!validateName($object['firstname']) || !validateName($object['lastname'])) {
        $response = array("success" => false, "message" => "Invalid first or last name.", 'data'=>null);
        http_response_code(400); 
        echo json_encode($response);
        exit;
    }
   
    if (!isset($object['gender']) || empty($object['gender']) || $object['gender']=="Select Gender") {
      
        header("HTTP/1.1 500 Internal Server Error");
        $response = array(
            'message' => 'Please enter your gender',
            'success' => false,
            'data'=>null,
        );
        http_response_code(400); 
        echo json_encode($response);
        exit;
    } 

    if (!isset($object['birthday']) || empty($object['birthday'])) {
        header("HTTP/1.1 500 Internal Server Error");
        $response = array(
            'message' => 'Please enter your birthday',
            'success' => false,
            'data'=>null,
        );
        http_response_code(400); 
        echo json_encode($response);
        exit;
    } 
    if(!validateBirthday($object['birthday']))
    {
        $response = array("success" => false, "message" => "Invalid date of birth", 'data'=>null);
        http_response_code(400); 
        echo json_encode($response);
        exit;
    }

    

 
    
    $response = array(
        'message' => 'Student added successfully.',
        'success' => true,  
        'data'=>$object
    );
    header('Content-Type: application/json');

    echo json_encode($response);
    exit;
} else {
  
    http_response_code(405);
    echo json_encode(array("success" => false, "message" => "Method not allowed."));
}




function validateName($name) {
    $nameRegex = "/^[A-Z][a-z]+(-[A-Z][a-z]+)?$/";
    return preg_match($nameRegex, $name);
}
function validateBirthday($birthday) {
  
    $dateParts = explode('-', $birthday);
    $year = intval($dateParts[0]);
    
    if ($year < 1950 || $year > 2010) {
        return false;
    }
    
    return true;
}

?>
