//service worker

const cacheName = 'files-v152';

self.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(cacheName).then(cache => {
      console.log('caching assets...');
      cache.addAll([
        '/',
        '/index.html',
        '/styles.css',
        '/scripts.js',
        '/server.js',
        './images/user.png',
        './images/notifs.png',
        './images/newnotif2.png',
        './images/trash-can.png',
        './images/edit.png',
        './images/favicon.ico',
        'https://pbs.twimg.com/media/F69ajTWWoAAJ7wL?format=jpg&name=large'
      ]);
    })
  );
});

//фільтрування старих кешів
self.addEventListener('activate', (event) =>{
  event.waitUntil(
    caches.keys().then(keys =>{
      return Promise.all(keys
        .filter(key => key !== cacheName)
        .map(key => caches.delete(key))
        )
    })
  )
});

//якщо файл не знаходить в кеші, то стягнути зі сторінки, якщо є інтернет
/*
self.addEventListener('fetch', (event) =>{
  event.respondWith(caches.match(event.request).then(cacheResponse =>{
    return cacheResponse || fetch(event.request);
  })
  );
});

*/