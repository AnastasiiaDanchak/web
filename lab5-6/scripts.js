const socket = io();

socket.on('connect', ()=>{
  console.log('connected successfully! ' + socket.id);
})

let studentsData = [];
let authorizedUsers = [];

document.addEventListener('DOMContentLoaded', function() {
  console.log('dom is loaded! fetching data from a db...');

  fetch('/student-data')
  .then(response => response.json())
  .then(data => {
    data.forEach(element => {
      let obj = {
        group: element.group,
        fname: element.fname,
        lname: element.lname,
        gender: element.gender,
        birthday: element.birthday,
        password: element.password,
        _id: element._id
      }
      addOrUpdateObjectByKey(studentsData, element._id, obj);
    });
    refreshTable();
  })
  .catch(error => {
    console.error(error);
  });
});

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('service-worker.js')
    .then((registration) => {
      console.log('sw registered:', registration.scope);
    })
    .catch((error) => {
      console.error('sw reg failed:', error);
    });
}

document.getElementById('add').onclick = function(){
   openPopup("add", null);
}

let t = " ";
let editBtn = null;
function openPopup(formTitle, btn) {
  document.getElementById("popup").style.display = "block";
  clearPopupFields();

  t = formTitle;
  editBtn = btn;
  if(formTitle === "add") document.getElementById('Add-Edit-Title').innerHTML = "Add a new student:"; 
  else if(formTitle === "edit"){
    document.getElementById('Add-Edit-Title').innerHTML = "Edit student:";
    //запис на формі даних студента, якого редагують -
    let btnRow = btn.closest('tr');
    document.getElementById('group-select').value = btnRow.cells[1].textContent;

    const names = btnRow.cells[2].textContent.split(" ");
    document.getElementById('inputFName').value = names[0]; //ім'я
    document.getElementById('inputLName').value = names[1]; //прізвище

    document.getElementById('gender-select').value = btnRow.cells[3].textContent;
    document.getElementById('inputBD').value = btnRow.cells[4].textContent.split('.').reverse().join('-');

  }
}

function clearPopupFields(){
  document.getElementById("group-select").value = "?";
  document.getElementById("inputFName").value = "";
  document.getElementById("inputLName").value = "";
  document.getElementById("gender-select").value = "?";
  document.getElementById("inputBD").value = "2000-01-01";
  document.getElementById("regPasswordInput").value = "";
}

function goToExecution(event){
  // t = add або edit
  if(t === "add"){
    submitPressed(event);
  }
  else if(t === "edit"){
    submitChangesPressed(event, editBtn);
  } 
}

function closePopup() {
  document.getElementById("popup").style.display = "none";
}

function submitPressed(event){
  event.preventDefault();

  //let sid = "s" + (studentData.length + 1).toString();
  let dataObject = {
    group: document.getElementById("group-select").value,
    fname: document.getElementById("inputFName").value,
    lname: document.getElementById("inputLName").value,
    gender: document.getElementById("gender-select").value,
    birthday: document.getElementById("inputBD").value,
    password: document.getElementById('regPasswordInput').value,
    todo: "add"
  };
  sendToServer(dataObject)
    .then(function(obj) {
      if(obj.success === true) {
        let table = document.getElementById('studentList');
        for (let i = table.rows.length - 1; i > 0; i--) table.deleteRow(i);
        refreshTable();

      } else if (obj.success === false) {
        if(obj.errors === 'no post-request') {
          alert('Не прийшов POST-запит. Спробуйте ще раз.');
        } else {
          console.log(obj.errors);
          alert(Object.values(obj.errors).join(', '));
        }
      }

      if(obj.success === true){
        clearPopupFields();
        closePopup();
      }
    })
    .catch(function(error) {
      console.log('Error:', error);
    });
}

function refreshTable(){
  let table = document.getElementById("studentList");

  for(const pair of studentsData){
    let tempObj = getItemByKey(studentsData, pair.value._id)
    //let tempObj = getItemByKey(studentData, ("s" + i));
    let row = document.createElement("tr")
    let c1 = document.createElement("td")
    let c2 = document.createElement("td")
    let c3 = document.createElement("td")
    let c4 = document.createElement("td")
    let c5 = document.createElement("td")
    let c6 = document.createElement("td")
    let c7 = document.createElement("td")

    let checkboxC1 = document.createElement("input");
    checkboxC1.type = "checkbox";
    c1.appendChild(checkboxC1);

    let inputC6 = document.createElement("input");
    inputC6.type = "checkbox";
    inputC6.id = 'userStatus';
    for(let user of authorizedUsers){
     if(pair.value._id === user._id) inputC6.checked = true; 
    }

    c6.appendChild(inputC6);

    c2.innerText = tempObj.group;
    c3.innerText = tempObj.fname + " " + tempObj.lname;
    c4.innerText = tempObj.gender;
    c5.innerText = tempObj.birthday.split('-').reverse().join('.');

    let button1 = document.createElement("button");
    button1.className = "editBtn"; 
    button1.addEventListener("click", function(){
      openPopup("edit", button1)
    })
    c7.appendChild(button1);
    let button2 = document.createElement("button");
    button2.className = "deleteBtn";
    button2.addEventListener("click", function(){
      openDelPopup(button2);
    })
    c7.appendChild(button2);
          
    row.appendChild(c1);
    row.appendChild(c2);
    row.appendChild(c3);
    row.appendChild(c4);
    row.appendChild(c5);
    row.appendChild(c6);
    row.appendChild(c7);
    row.id = pair.value._id;


    let myDiv = document.getElementById("tableSt");
    let currentHeight = parseInt(myDiv.style.height, 10) || myDiv.clientHeight;
    let newHeight = currentHeight + 62;
    myDiv.style.height = newHeight + "px";

    table.querySelector("tbody").appendChild(row);
  }
  closePopup();
}

function openDelPopup(btn){
  document.getElementById('delPopup').style.display = "block";

  let row = btn.closest("tr");

  document.getElementById('delWarning').innerHTML = "Are you sure you want delete user?</b>";
  document.getElementById('deleleUserBtn').onclick = function(){ deleteStudent(row) };
}

function resetKeyAndId(oldKey, newKeyAndId) { 
  let objectString = getItemByKey(studentsData, oldKey);

  if (objectString !== null) {
      objectString.id = newKeyAndId;
      removeItemByKey(studentsData, oldKey);
      addOrUpdateObjectByKey(studentsData, newKeyAndId, objectString);
      
  } else {
      console.log("Об'єкт з таким ключем не знайдено.");
  }
}

function deleteStudent(row){
  let table = document.getElementById('studentList');

  //let delObj = JSON.parse(studentData.getItem("s"+row.rowIndex));
  // let delObj = getItemByKey(studentData, ("s"+row.rowIndex))
  let delObj = getItemByKey(studentsData, row.id);
  delObj.todo = "delete";
  sendToServer(delObj)
    .then(function(obj){
      if(obj.success === true){
        console.log("User called " + obj.data.fname + " " + obj.data.lname + " has been deleted!");
      }
      else{
        console.log(obj.errors);
        alert(Object.values(obj.errors).join(', '));
      }
    })
  removeItemByKey(studentsData, delObj._id)
  table.deleteRow(row.rowIndex);

  document.getElementById('delPopup').style.display = "none";
}

function closeDelPopup(){
  document.getElementById('delPopup').style.display = "none";
}

let notifBar = document.getElementById("notifBar");
let notifBlock = document.getElementById("notifBlock");

notifBar.addEventListener("mouseover", function() {
    notifBlock.style.display = "block";
});

notifBar.addEventListener("mouseout", function() {
    notifBlock.style.display = "none";
});

let profileBar = document.getElementById("profileBar");
let profileBlock = document.getElementById("profileBlock");

profileBar.addEventListener("mouseover", function() {
  profileBlock.style.display = "block";
});

profileBar.addEventListener("mouseout", function() {
  profileBlock.style.display = "none";
});

function submitChangesPressed(event, btn){
  event.preventDefault();
  let btnRow = btn.closest('tr');

  let dataObject = getItemByKey(studentsData, btnRow.id);
  dataObject.group = document.getElementById("group-select").value
  dataObject.fname = document.getElementById("inputFName").value
  dataObject.lname = document.getElementById("inputLName").value
  dataObject.gender = document.getElementById("gender-select").value
  dataObject.birthday = document.getElementById("inputBD").value
  dataObject.password = document.getElementById("regPasswordInput").value
  dataObject.todo = 'edit';
  
  sendToServer(dataObject)
    .then(function(obj) {
      console.log(obj);
      if(obj.success === true) {
        let table = document.getElementById('studentList');
        for (let i = table.rows.length - 1; i > 0; i--) table.deleteRow(i);
        refreshTable();
      } else if (obj.success === false) {
        if(obj.errors === 'no post-request') {
          alert('Не прийшов POST-запит. Спробуйте ще раз.');
        } else {
          console.log(obj.errors);
          alert(Object.values(obj.errors).join(', '));
        }
      }

      if(obj.success === true){
        clearPopupFields();
        closePopup();
      }
    })
    .catch(function(error) {
      console.log('Error:', error);
    });

}

function sendToServer(jsonStr){

  if(jsonStr.todo == "add" || jsonStr.todo == "edit"){
    return fetch("/check-student", {
      "method": "POST",
      "headers":{
        "Content-Type": "application/json; charset=utf-8"
      },
      "body": JSON.stringify(jsonStr)
    }).then(function(response){
      return response.json();
    }).then(function(obj){
      console.log(obj);
      if(obj.success === true){
        delete obj.data.todo;
        addOrUpdateObjectByKey(studentsData, (obj.data._id).toString(), obj.data)
      }
      return obj;
    })
  }
  else if(jsonStr.todo == "delete"){
    return fetch("/delete-student", {
      "method": "POST",
      "headers":{
        "Content-Type": "application/json; charset=utf-8"
      },
      "body": JSON.stringify(jsonStr)
    }).then(function(response){
      return response.json();
    }).then(function(obj){
      return obj;
    })
  }
}


// Функція для видалення елемента за ключем з масиву
function removeItemByKey(arr, key) {
  for (var i = 0; i < arr.length; i++) {
      if (arr[i].key === key) {
          arr.splice(i, 1);
          return;
      }
  }
}

// Функція для отримання значення за ключем з масиву
function getItemByKey(arr, key) {
  for (var i = 0; i < arr.length; i++) {
      if (arr[i].key === key) {
          return arr[i].value;
      }
  }
  return null; // Повертаємо null, якщо ключ не знайдено
}

function addOrUpdateObjectByKey(arr, key, value) {
  // Перевіряємо, чи є у масиві об'єкт з ключем, який ми шукаємо
  var found = false;
  for (var i = 0; i < arr.length; i++) {
      if (arr[i].key === key) {
          // Якщо знайдено об'єкт з таким ключем, замінюємо його значення
          arr[i].value = value;
          found = true;
          break;
      }
  }
  
  // Якщо об'єкт з таким ключем не був знайдений, додаємо новий об'єкт до масиву
  if (!found) {
      arr.push({ key: key, value: value });
  }
}

let activeUser = {};
document.getElementById('authorizeUser').onclick = function(){
  let inputFullName = document.getElementById('inputFullName').value;
  let inputPassword = document.getElementById('inputPassword').value;
  for(const pair of studentsData){
    let userFullName = pair.value.fname + ' ' + pair.value.lname;
    if(userFullName != inputFullName) continue;
    if(pair.value.password != inputPassword) continue;
    
    //перевірку пройдено
    document.getElementById('loginPopup').style.display = 'none'
    document.getElementById('black-bg').style.display = 'none'
    document.getElementById('profName').innerHTML = '<b>' + userFullName + '</b>';
    document.getElementById('anotherProfName').innerHTML = '<b>' + userFullName + '</b>';
    activeUser = pair.value;

    let authobj = {
      socketId: socket.id,
      _id: pair.key
    }
    //надсилаємо на сервер об'єкт, який має айді сокета і айді користувача, за якого на цьому сокеті зайшли
    socket.emit('userAuth', authobj)
    return;
  }

  alert('wrong login data!')
}

let chatsData = [];
function openMessagesPage(){
  //event.preventDefault();
  document.getElementById('messagesPage').style.display = 'block';
  document.getElementById('active-chat-messages').innerHTML = '';
  let chatroomobjs = document.getElementsByClassName('chatroom-obj');
  if(chatroomobjs.length > 0){
    let chatroomobjarray = Array.from(chatroomobjs);
    chatroomobjarray.forEach(function(element){
      element.remove();
    })
  }

  let memberscontainers = document.getElementsByClassName('img-and-name-container')
  if(memberscontainers.length > 0){
    let containersarray = Array.from(memberscontainers);
    containersarray.forEach(function(element){
      element.remove();
    })
  }
  document.getElementById('title-activechat').innerHTML = 'Select a chat:'

  getChatsData().then((data) => {
    for(const pair of chatsData){
      if( checkForValue(pair.value.participants, activeUser._id)) {
        let chatDiv = document.createElement('div');
        chatDiv.id = pair.key
        chatDiv.className = 'chatroom-obj'
        if(pair.value.chatname === 'default'){
          //якщо назва чату default, то назва чату - імена інших учасників у ньому через кому
          for(const uid of pair.value.participants){
            let student = getItemByKey(studentsData, uid)
            if(uid !== activeUser._id) chatDiv.innerHTML += student.fname + ' ' + student.lname + ', ';
          }
          let text = chatDiv.innerHTML;
          text = text.slice(0, -2);
          chatDiv.innerHTML = text;
          
        } else chatDiv.innerHTML = pair.value.chatname
        chatDiv.onclick = function() {openChat(pair.key)}
        document.getElementById('chats-header').appendChild(chatDiv);
      } 
    }


  }).catch((error) => {
    console.error('Сталася помилка:', error);
  });


}

function checkForValue(arr, value){

  for(let i = 0; i < arr.length; i++){
    if( arr[i] === value){
      return true
    }
  }
  return false
}

//показати чат при натисканні на дів блок зліва (в chat rooms)
document.getElementsByClassName('chatroom-obj').onclick = function(){

}

function createServerMessageBlock(message){
  const msgContainer = document.createElement('div');
  msgContainer.id = 'servermsg-div'

  const msgText = document.createElement('p')
  msgText.id = 'servermsg-txt'
  msgText.innerHTML = message
  msgContainer.appendChild(msgText);
  return msgContainer;
}

function createMessageBlock(name, message, right) {
  // Створити div-елемент для контейнера повідомлення
  const msgContainer = document.createElement('div');
  msgContainer.classList.add('msg-container');
  if(right) msgContainer.classList.add('right');

  // Створити div-елемент для контейнера фото і імені
  const imgAndNameContainer = document.createElement('div');
  imgAndNameContainer.classList.add('img-and-name-container');

  // Створити елемент img для фото
  const img = document.createElement('img');
  img.classList.add('msg-img');
  img.src = './images/user.png';

  // Створити елемент label для імені
  const nameLabel = document.createElement('label');
  nameLabel.id = 'msgnamelabel';
  nameLabel.style.fontSize = '1vh';
  nameLabel.textContent = name;

  // Додати елементи до контейнера фото і імені
  imgAndNameContainer.appendChild(img);
  imgAndNameContainer.appendChild(nameLabel);

  // Створити div-елемент для тексту повідомлення
  const textBubble = document.createElement('div');
  textBubble.classList.add('text-bubble');
  if(right) textBubble.classList.add('right');
  else textBubble.classList.add('left');
  textBubble.textContent = message;

  // Додати елементи до контейнера повідомлення
  if(right){
    msgContainer.appendChild(textBubble);
    msgContainer.appendChild(imgAndNameContainer);
  } else{
    msgContainer.appendChild(imgAndNameContainer);
    msgContainer.appendChild(textBubble);
  }

  // Повернути створений HTML-блок
  return msgContainer;
}


function isStringInArray(array, searchString) {
  // Перевіряємо кожен елемент масиву
  for (let i = 0; i < array.length; i++) {
      // Якщо елемент масиву рівний шуканій стрічці, повертаємо true
      if (array[i] === searchString) {
          return true;
      }
  }
  // Якщо не знайдено співпадінь, повертаємо false
  return false;
}

function closeMessagesPage(){
  document.getElementById('messagesPage').style.display = 'none';
}

function getChatsData(){
  return fetch('/chat-data')
  .then(response => response.json())
  .then(data => {
    data.forEach(element => {
      let obj = {
        _id: element._id,
        chatname: element.chatname,
        participants: element.participants,
        messages: element.messages
      }
      addOrUpdateObjectByKey(chatsData, element._id, obj);
    });
    return data;
  })
  .catch(error => {
    console.error(error);
    return null;
  });
}

let activeChat = null;
function openChat(_id){
  getChatsData().then(data =>{
    activeChat = getItemByKey(chatsData, _id);
    //видалення повідомлень з іншого чату
    document.getElementById('active-chat-messages').innerHTML = '';
    let membersDiv = document.getElementById('chat-members')
    var elements = membersDiv.getElementsByClassName('img-and-name-container')
    while(elements.length > 0) membersDiv.removeChild(elements[0]);

    for(let p of activeChat.participants){
      let data = getItemByKey(studentsData, p)
      if (data === null) continue;
      
      const imgAndNameContainer = document.createElement('div');
      imgAndNameContainer.classList.add('img-and-name-container');
      imgAndNameContainer.id = p + '_status';

      let row = document.getElementById(p);
      let userStatusBox = row.querySelector('#userStatus');

      const img = document.createElement('img');
      img.classList.add('msg-img');
      if(userStatusBox.checked === true) img.src = './images/user-online.png';
      else img.src = './images/user.png';

      const nameLabel = document.createElement('label');
      nameLabel.id = 'msgnamelabel';
      nameLabel.style.fontSize = '1vh';
      
      let student = getItemByKey(studentsData, p);
      nameLabel.textContent = student.fname;

      imgAndNameContainer.appendChild(img);
      imgAndNameContainer.appendChild(nameLabel);
      document.getElementById('chat-members').insertBefore(imgAndNameContainer, document.getElementById('addStudentToChat'));
    }

    let chat = getItemByKey(chatsData, _id);
    for(const msg of chat.messages){
      if(msg.sender === '-'){
        let msgdiv = createServerMessageBlock(msg.text);
        document.getElementById('active-chat-messages').appendChild(msgdiv);
        document.getElementById('active-chat-messages').appendChild(document.createElement('br'));
      }
      else{
        let senderInfo = getItemByKey(studentsData, msg.sender);
        let msgdiv;
        if(senderInfo._id === activeUser._id){
          msgdiv = createMessageBlock(senderInfo.fname, msg.text, true)
        } else {
          msgdiv = createMessageBlock(senderInfo.fname, msg.text, false)
        }
        document.getElementById('active-chat-messages').appendChild(msgdiv);
        document.getElementById('active-chat-messages').appendChild(document.createElement('br'));
      }
    }
    document.getElementById('title-activechat').innerHTML = 'Active chat: ' + document.getElementById(_id).innerHTML; 
  })
  
}

function sendMessage(event){
  event.preventDefault();

  if(activeChat === null){
    alert('select a chat room first!')
    return;
  }

  let msgobj = {
    sender: activeUser._id,
    text: document.getElementById('enter-message').value,
    chat: activeChat._id
  }

  socket.emit('sendMessage', msgobj);

  let msgdiv = createMessageBlock(activeUser.fname, document.getElementById('enter-message').value, true);
  document.getElementById('active-chat-messages').appendChild(msgdiv);
  document.getElementById('active-chat-messages').appendChild(document.createElement('br'));

  document.getElementById('enter-message').value = '';
}

socket.on('receiveMessage', data => {
  if(activeChat._id === data.chat){
    //якщо чат, в якому прийшло повідомлення, відкритий
    let msgdiv = createMessageBlock(getItemByKey(studentsData, data.sender).fname, data.text, false);

    document.getElementById('active-chat-messages').appendChild(msgdiv);
    document.getElementById('active-chat-messages').appendChild(document.createElement('br'));
  } else {
    //якщо відкритий інший чат
  }

  getChatsData();
})

socket.on('now-online', data => {
  let row = document.getElementById(data._id);
  if(!row) console.log('didnt get the row data')
  else{
    let userStatusBox = row.querySelector('#userStatus');
    if(!userStatusBox) console.log('didnt get the checkbox data')
    else userStatusBox.checked = true;
    authorizedUsers.push(data);
    let status = document.getElementById(data._id + '_status');
    if(status){
      status.innerHTML = '';
      const img = document.createElement('img');
      img.classList.add('msg-img');
      img.src = './images/user-online.png';

      let student = getItemByKey(studentsData, data._id);
      const nameLabel = document.createElement('label');
      nameLabel.id = 'msgnamelabel';
      nameLabel.style.fontSize = '1vh';
      nameLabel.textContent = student.fname;
    
      // Додати елементи до контейнера фото і імені
      status.appendChild(img);
      status.appendChild(nameLabel);
    }
  }
})

socket.on('now-offline', data =>{
  let row = document.getElementById(data._id);
  if(!row) console.log('didnt get the row data')
  else{
    let userStatusBox = row.querySelector('#userStatus');
    if(!userStatusBox) console.log('didnt get the checkbox data')
    else userStatusBox.checked = false;
    authorizedUsers.pop(data);
    let status = document.getElementById(data._id + '_status');
    if(status){
      //status.style.border = '1px solid yellowgreen';
      status.innerHTML = '';
      const img = document.createElement('img');
      img.classList.add('msg-img');
      img.src = './images/user.png';
    
      // Створити елемент label для імені
      let student = getItemByKey(studentsData, data._id);
      const nameLabel = document.createElement('label');
      nameLabel.id = 'msgnamelabel';
      nameLabel.style.fontSize = '1vh';
      nameLabel.textContent = student.fname;
    
      // Додати елементи до контейнера фото і імені
      status.appendChild(img);
      status.appendChild(nameLabel);
    }
  }
})

document.getElementById('addStudentToChat').onclick = function(){
  if (activeChat === null){
    alert('select a chat first!')
    return
   }
  document.getElementById('black-bg').style.display = 'block';
  document.getElementById('addStudentPage').style.display = 'block';

  let selectElement = document.getElementById('selectStudentsToAdd')

 for(let student of studentsData){
  if(isStringInArray(activeChat.participants, student.value._id)) continue;
  else{
    let newOption = document.createElement('option');
    newOption.text = student.value.fname + ' ' + student.value.lname;
    newOption.value = student.value._id;
    selectElement.appendChild(newOption);
  }
 }

}

//відміна додавання студента в чат
document.getElementById('declineStudentAddition').onclick = function(){

  let selectElement = document.getElementById('selectStudentsToAdd')
  while (selectElement.options.length > 0) {
    selectElement.remove(0);
  }

  document.getElementById('black-bg').style.display = 'none';
  document.getElementById('addStudentPage').style.display = 'none';
}

//підтвердження додавання студента в чат
document.getElementById('confirmStudentAddition').onclick = function(){

  let selectElement = document.getElementById('selectStudentsToAdd')
  let selectedOption = selectElement.options[selectElement.selectedIndex];
  while (selectElement.options.length > 0) {
    selectElement.remove(0);
  }

  document.getElementById('black-bg').style.display = 'none';
  document.getElementById('addStudentPage').style.display = 'none';

  let obj = {
    chat: activeChat._id,
    newparticipant: selectedOption.value
  }

  socket.emit('addNewParticipant', obj)
}

socket.on('updateChatData', data => {
  //addOrUpdateObjectByKey(chatsData, data, data);

  if(findElementByKey(chatsData, data) !== null){
    console.log(activeUser.fname + ' ' + activeUser.lname);

    closeMessagesPage();
    openMessagesPage();

    let chat = getItemByKey(chatsData, data);
    console.log(chat);
    if(activeChat === chat){
      let oldmsgs = document.getElementsByClassName('msg-container')
      if(oldmsgs.length > 0){
        let msgarray = Array.from(oldmsgs);
        msgarray.forEach(function(element) {
          element.remove();
        });
    
        let activeChatDiv = document.getElementById('active-chat-messages')
        let brTags = Array.from(activeChatDiv.getElementsByTagName('br'));
        brTags.forEach(function(brTag) {
          brTag.remove();
        });
    
        let servermsgDivs = document.querySelectorAll('#servermsg-div');
        servermsgDivs.forEach(function(element){
          element.remove();
        })
        for(let i = 0; i < 4; i++)activeChatDiv.appendChild(document.createElement('br'));
      }
    }
  }

})

socket.on('newChat', data=>{
  addOrUpdateObjectByKey(chatsData, data._id, data);
  console.log('hello from newChat event! :>')
  closeMessagesPage();
  openMessagesPage();
})

function findElementByKey(arr, key) {
  // Assuming 'chats' is an array of chat documents (not a Query object)
  for (const element of arr) {
    if (element.key.toString() === key.toString()) {
      return element;
    }
  }
  return null;
}

function findStringInArray(array, str){
  for(const element of array){
    if(element.toString() === str.toString()) return element;
  }
  return null;
}
document.getElementById('removeStudentFromChat').onclick = function(){
  if (activeChat === null){
    alert('select a chat first!')
    return
  }
  document.getElementById('black-bg').style.display = 'block';
  document.getElementById('removeStudentPage').style.display = 'block';

  let selectElement = document.getElementById('selectStudentsToRemove')

 for(let student of studentsData){
  if(!isStringInArray(activeChat.participants, student.value._id)) continue;
  else{
    if(student.value._id === activeUser._id) continue;
    let newOption = document.createElement('option');
    newOption.text = student.value.fname + ' ' + student.value.lname;
    newOption.value = student.value._id;
    selectElement.appendChild(newOption);
  }
 }
}

//підтвердження додавання студента в чат
document.getElementById('confirmStudentRemoval').onclick = function(){

  let selectElement = document.getElementById('selectStudentsToRemove')
  let selectedOption = selectElement.options[selectElement.selectedIndex];
  while (selectElement.options.length > 0) {
    selectElement.remove(0);
  }

  document.getElementById('black-bg').style.display = 'none';
  document.getElementById('removeStudentPage').style.display = 'none';

  let obj = {
    chat: activeChat._id,
    participant: selectedOption.value
  }

  socket.emit('removeParticipant', obj)
}

document.getElementById('declineStudentRemoval').onclick = function(){

  let selectElement = document.getElementById('selectStudentsToRemove')
  while (selectElement.options.length > 0) {
    selectElement.remove(0);
  }
  
  document.getElementById('black-bg').style.display = 'none';
  document.getElementById('removeStudentPage').style.display = 'none';
}


function openCreateChatPopup(){
  document.getElementById('chatRoomPopup').style.display = 'flex';
  document.getElementById('black-bg').style.display = 'block';
  const selectElement = document.getElementById('selectStudentsToNewChat');

  for(let s of studentsData){
    if(s.key.toString() === activeUser._id) continue;
    let newOption = document.createElement('option');
    newOption.value = s.key.toString();
    newOption.text = s.value.fname + ' ' + s.value.lname;
    selectElement.appendChild(newOption);
  }


  //selectStudentsToNewChat
}

//при натисканні create не забути додати до цього масиві айді активного юзера!!!
let addedStudentsToNewChat = [];
document.getElementById('addStudentToNewChat').onclick = function(){
  const selectElement = document.getElementById('selectStudentsToNewChat');
  let _idToAdd = selectElement[selectElement.selectedIndex].value;
  
  let student = getItemByKey(studentsData, _idToAdd);
  addedStudentsToNewChat.push(student._id);

  selectElement.remove(selectElement.selectedIndex);

  let addedStudentLabel = document.createElement('label')
  addedStudentLabel.innerHTML = student.fname + ' ' + student.lname;
  addedStudentLabel.id = student.fname + student.lname + student._id;
  let removeBtn = document.createElement('button')
  removeBtn.innerHTML = 'Remove';
  removeBtn.id = 'removeStudentFromNewChat';
  removeBtn.onclick = function() { removeStudentFromNewChat(removeBtn, student)}
  document.getElementById('addedStudents');

  document.getElementById('addedStudents').appendChild(addedStudentLabel);
  document.getElementById('addedStudents').appendChild(removeBtn);
  document.getElementById('addedStudents').appendChild(document.createElement('br'));
  document.getElementById('addedStudents').appendChild(document.createElement('br'));
}

function removeStudentFromNewChat(btn, student){
  const addedStudentsDiv = document.getElementById('addedStudents');
  //const elementToRemove = addedStudentsDiv.getElementById(_id);
  var elementToRemove = addedStudentsDiv.querySelector('#' + student.fname + student.lname + student._id);
  if(elementToRemove) addedStudentsDiv.removeChild(elementToRemove);
  else console.log('didnt find the label :<')
  addedStudentsDiv.removeChild(btn);

  const index = addedStudentsToNewChat.findIndex(element => element.toString() === student._id.toString());
  if(index !== -1) addedStudentsToNewChat.splice(index, 1)

  const selectElement = document.getElementById('selectStudentsToNewChat');
  let option = document.createElement('option')
  option.value = student._id.toString();
  option.text = student.fname + ' ' + student.lname;
  selectElement.appendChild(option);
}


document.getElementById('closeCreateChatPopup').onclick = function(){
  const selectElement = document.getElementById('selectStudentsToNewChat');
  while (selectElement.firstChild) {
    selectElement.removeChild(selectElement.firstChild);
  }
  let addedStudentsDiv = document.getElementById('addedStudents');
  if (addedStudentsDiv) {
    var labels = addedStudentsDiv.getElementsByTagName('label');
    var buttons = addedStudentsDiv.getElementsByTagName('button');
    var brs = addedStudentsDiv.getElementsByTagName('br');

    for (var i = labels.length - 1; i >= 0; i--) {
        labels[i].parentNode.removeChild(labels[i]);
    }

    for (var i = buttons.length - 1; i >= 0; i--) {
        buttons[i].parentNode.removeChild(buttons[i]);
    }

    for (var i = brs.length - 1; i >= 0; i--) {
        brs[i].parentNode.removeChild(brs[i]);
    }
  } else {
    console.log("Div-блок з вказаним id не знайдено?");
  }
  for(let i = 0; i < 3; i++) addedStudentsDiv.appendChild(document.createElement('br'))

  document.getElementById('chatRoomPopup').style.display = 'none';
  document.getElementById('black-bg').style.display = 'none';
}

document.getElementById('createNewChat').onclick = function(){
  const selectElement = document.getElementById('selectStudentsToNewChat');
  while (selectElement.firstChild) {
    selectElement.removeChild(selectElement.firstChild);
  }
  addedStudentsToNewChat.push(activeUser._id)

  let chatname = document.getElementById('chatCreateName').value;
  if(chatname === '') chatname = 'default';

  let obj = {
    chatname: chatname,
    participants: addedStudentsToNewChat,
    messages: []
  }
  socket.emit('createNewChat', obj);
  addedStudentsToNewChat = [];
  let addedStudentsDiv = document.getElementById('addedStudents');
  if (addedStudentsDiv) {
    var labels = addedStudentsDiv.getElementsByTagName('label');
    var buttons = addedStudentsDiv.getElementsByTagName('button');
    var brs = addedStudentsDiv.getElementsByTagName('br');

    for (var i = labels.length - 1; i >= 0; i--) {
        labels[i].parentNode.removeChild(labels[i]);
    }

    for (var i = buttons.length - 1; i >= 0; i--) {
        buttons[i].parentNode.removeChild(buttons[i]);
    }

    for (var i = brs.length - 1; i >= 0; i--) {
        brs[i].parentNode.removeChild(brs[i]);
    }
  } else {
    console.log("Div-блок з вказаним id не знайдено?");
  }
  for(let i = 0; i < 3; i++) addedStudentsDiv.appendChild(document.createElement('br'))
  document.getElementById('chatRoomPopup').style.display = 'none';
  document.getElementById('black-bg').style.display = 'none';
}